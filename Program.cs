﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace TriangleoldDotNet
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var triangleObj = new Program();
            triangleObj.Pascal3();

        }

        public void Pascal3()
        {
            int[][] TriangleArr = new int[6][];
            var ix = 0;

            while(ix<=5) //create 2nd dimenaion array for each row. IE Cols
            {
                TriangleArr[ix] = new int[ix + 1];
                ix++;
            }
            TriangleArr[0][0] = 1;
            for (var row = 0; row < TriangleArr.Count()-1; row++)
            {
                var strRow = "";
                for (var col = 0; col < TriangleArr[row].Length; col++)
                {
                    TriangleArr[row + 1][col] = TriangleArr[row + 1][col] + TriangleArr[row][col];
                    TriangleArr[row + 1][col + 1] = TriangleArr[row + 1][col + 1] + TriangleArr[row][col] ;
                    strRow = strRow+TriangleArr[row][col].ToString();
                }
                Console.WriteLine(strRow);
            }
            Console.ReadLine();
        }
    }
}
